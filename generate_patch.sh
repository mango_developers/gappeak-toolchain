#!/bin/sh
#
# 1 - Original directory
# 2 - Modified directory
# 3 - File with the list of looked files / directories (no empty lines)
# 4 - Patch file to be created

while read f ; do diff -bNu $1/$f $2/$f; done < $3 > $4

